import pickle
import string
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from PIL.ExifTags import TAGS
from PIL import Image, ImageDraw
from collections import OrderedDict


def read_image(imname):
	angle = 0
	pil_img = Image.open(imname)
	try:
		angle_from_orientation = {1:0, 8:90, 3:180, 6:270}
		info = pil_img._getexif()
		if info is not None:
			for tag, value in info.items():
				key = TAGS.get(tag)
				if key == 'Orientation':
					angle = angle_from_orientation[value]
					if angle:
						angle = 360-angle
	except:
		pass
	return pil_img, angle

def get_gtboxes(imname, angle):

	fp = open(imname.replace(imname[-3:], "xml"))
	boxes = []
	classes = []

	text = fp.read()
	fp.close()
	parsed = BeautifulSoup(text,"lxml")
	for object in parsed.find_all("object"):
		classname = object.find("name").string
		classname = str(classname)
		objectbnd = object.find("bndbox")
		xmin = objectbnd.find("xmin").string
		ymin = objectbnd.find("ymin").string
		xmax = objectbnd.find("xmax").string
		ymax = objectbnd.find("ymax").string

		ymin,ymax,xmin,xmax = int(ymin),int(ymax), int(xmin),int(xmax)

		boxes.append([xmin,ymin,xmax,ymax])
		classes.append(classname)

	return boxes, classes

def get_gtboxes_csv(imname, csv_path,group_level):

	boxes=[]
	classes=[]	
	df = pd.read_csv(csv_path) 
	df = df[(df['delete_status']==0)]
	#img_id = (''.join(elem+'_' for elem in imname.split('/')[-1].split('_')[2:])).strip('_')
	img_id = imname.split('/')[-1]

	df_img_annots = df[(df['img_id']==img_id)]
	for i,rows in df_img_annots.iterrows():
		if  'shelf' in rows['classname'].lower():
			print ('shelf in gt ')
			continue
		xmin = int(float(rows['xmin']))
		ymin = int(float(rows['ymin']))
		xmax = int(float(rows['xmax']))
		ymax = int(float(rows['ymax']))
		if xmin >= xmax or ymin >= ymax:
			continue
		boxes.append([xmin,ymin,xmax,ymax])
		#uncomment for pop classifier
		if group_level:
			classes.append(rows['groupname'].lower())
		else:
			classes.append(rows['groupname'].lower()+'_'+rows['classname'].lower())
	return boxes,classes	


def calculate_metrics_detection(gt_boxes, pred_boxes, ovthresh=0.5, validity=None, distances=None, img_path=None):
	'''
	GT Boxes is a list of all tagged bboxes 
	Pred Boxes is list of all predicted boxes as currently only fg-bg
	'''
	global_cnt = 0
	save_fp_patches = False
	save_tp_patches = False
	# img = imread(img_path)
	# imid = img_path.split('/')[-1].split('.')[0]
	## convert both to an array
	gt_boxes = np.asarray(gt_boxes)
	pred_boxes = np.asarray(pred_boxes)

	# print(gt_boxes.shape,pred_boxes.shape)

	nd = len(pred_boxes)
	npos = len(gt_boxes)
	npos_valid = len(gt_boxes)

	tp = np.zeros(nd)
	fp = np.zeros(nd)	

	detected_flag = [0]*npos
	d = 0 		# index for number of prediction bboxes

	if len(gt_boxes) > 0:
		for bb in pred_boxes:
			# intersection
			ixmin = np.maximum(gt_boxes[:, 0], bb[0])
			iymin = np.maximum(gt_boxes[:, 1], bb[1])
			ixmax = np.minimum(gt_boxes[:, 2], bb[2])
			iymax = np.minimum(gt_boxes[:, 3], bb[3])
			iw = np.maximum(ixmax - ixmin + 1., 0.)
			ih = np.maximum(iymax - iymin + 1., 0.)
			inters = iw * ih

			# union
			uni = ((bb[2] - bb[0] + 1.) * (bb[3] - bb[1] + 1.) +
					(gt_boxes[:, 2] - gt_boxes[:, 0] + 1.) *
					(gt_boxes[:, 3] - gt_boxes[:, 1] + 1.) - inters)

			overlaps = inters / uni
			ovmax = np.max(overlaps)
			jmax = np.argmax(overlaps)

			if ovmax > ovthresh:
				if not detected_flag[jmax]:
					tp[d] = 1.
					detected_flag[jmax] = 1
					if save_tp_patches:
						## Save predicted boxes
						fname = plot_folder + 'TPs/{}_{}.jpg'.format(imid,global_cnt)
						bb = [int(b) for b in bb]
						imsave(fname,img[bb[1]:bb[3],bb[0]:bb[2]])
						global_cnt+=1

				else:
					## Dont count multiple matches for same detection as FP
					# pass 
					fp[d] = 1.
					if save_fp_patches:
						fname = plot_folder + 'FPs/{}_{}.jpg'.format(imid,global_cnt)
						bb = [int(b) for b in bb]
						imsave(fname,img[bb[1]:bb[3],bb[0]:bb[2]])
						global_cnt+=1			
			else:
				## Check if its completely inside 

				fp[d] = 1.
				if save_fp_patches:
					fname = plot_folder + 'FPs/{}_{}.jpg'.format(imid,global_cnt)
					bb = [int(b) for b in bb]
					imsave(fname,img[bb[1]:bb[3],bb[0]:bb[2]])
					global_cnt+=1
			d += 1

		detected_flag = np.asarray(detected_flag)
		# print(validity, detected_flag, npos_valid)
		# npos_valid -= (validity[detected_flag==0]==0).sum()
		# print(npos_valid)

	else:
		num_bbs = len(pred_boxes)
		fp[d: d+num_bbs] = 1	# add false positive flag to each detection
		d+= num_bbs				# update number of detections
		for bb in pred_boxes:
			if save_fp_patches:
				fname = plot_folder + 'FPs/{}_{}.jpg'.format(imid,global_cnt)
				bb = [int(b) for b in bb]
				imsave(fname,img[bb[1]:bb[3],bb[0]:bb[2]])
				global_cnt+=1		

	# compute precision recall
	ffp = np.sum(fp)
	ttp = np.sum(tp)

	# print "Number of detections: ", nd
	# print "Number of GT objects: ", npos
	# print "Number of TP and FP: ", ttp, ffp

	rec = ttp / float(npos)
	prec = ttp / np.maximum(ttp + ffp, np.finfo(np.float64).eps)

	return ttp,ffp,npos

def calculate_metrics_class(class_rec, ovthresh):

	global global_cnt
	num_imgs = len(class_rec)
	nd = 0
	for img_no in range(num_imgs):	
		bbs = class_rec[img_no]['bbox']
		nd += len(bbs)

	tp = np.zeros(nd)
	fp = np.zeros(nd)

	tpi = np.zeros(num_imgs)
	fpi = np.zeros(num_imgs)
	npi = np.zeros(num_imgs)

	d = 0 		# number of prediction bboxes
	npos = 0	# number of gt boxes
	npos_valid = 0 # valid gt boxes

	for img_no in range(num_imgs):
		# img = imread(img_path[img_no])
		# img = images[img_no]
		# imid = img_path[img_no].split('/')[-1].split('.')[0]
		
		BBGT = class_rec[img_no]['gt']
		BBGT = np.asarray(BBGT, dtype=np.float32)
		bbs = class_rec[img_no]['bbox']
		detected_flag = class_rec[img_no]['detected']

		if len(BBGT) > 0:
			
			# ## BBGT was xmin, ymin, width, height; so converting
			# BBGT[:,2] = BBGT[:,2] + BBGT[:,0]
			# BBGT[:,3] = BBGT[:,3] + BBGT[:,1]
			npos += BBGT.shape[0]
			npos_valid += BBGT.shape[0]
			npi[img_no] = BBGT.shape[0]

			for bb in bbs:
				bb = np.asarray(bb, dtype=np.float32)

				if BBGT.size > 0:
					# compute overlaps
					# intersection
					ixmin = np.maximum(BBGT[:, 0], bb[0])
					iymin = np.maximum(BBGT[:, 1], bb[1])
					ixmax = np.minimum(BBGT[:, 2], bb[2])
					iymax = np.minimum(BBGT[:, 3], bb[3])
					iw = np.maximum(ixmax - ixmin + 1., 0.)
					ih = np.maximum(iymax - iymin + 1., 0.)
					inters = iw * ih

					# union
					uni = ((bb[2] - bb[0] + 1.) * (bb[3] - bb[1] + 1.) +
						   (BBGT[:, 2] - BBGT[:, 0] + 1.) *
						   (BBGT[:, 3] - BBGT[:, 1] + 1.) - inters)

					overlaps = inters / uni
					ovmax = np.max(overlaps)
					jmax = np.argmax(overlaps)

				if ovmax > ovthresh:
					if not detected_flag[jmax]:
						tp[d] = 1.
						detected_flag[jmax] = 1
						tpi[img_no] += 1
					else:
						fp[d] = 1.
						fpi[img_no] += 1
						
				else:
					fp[d] = 1.
					fpi[img_no] += 1					
				
				d += 1

			detected_flag = np.asarray(detected_flag)

		else:

			num_bbs = len(bbs)
			fp[d: d+num_bbs] = 1	# add false positive flag to each detection
			d+= num_bbs				# update number of detections

			fpi[img_no] += num_bbs				

	info = np.stack([tpi,fpi,npi])

	# compute precision recall
	ffp = np.sum(fp)
	ttp = np.sum(tp)

	rec = ttp / float(npos_valid)
	prec = ttp / np.maximum(ttp + ffp, np.finfo(np.float64).eps)

	# return rec, prec, ap
	return rec, prec, ttp, ffp, npos_valid, info

def valid_f1(prec,rec,sequence=True):
	f1 = (2*prec*rec)/(prec+rec)
	if sequence:
		f1[prec+rec==0] = 0

	return f1

def get_acc_obj(outputs, targets, num_classes, ovthresh=0.5):
	
	class_recs = OrderedDict()
	for classid in range(num_classes):
		class_recs[classid] = OrderedDict()
	
	### Accumulate values for each class (from other way round) coz we need calculate metrics for each class first
	for num,(output,target) in enumerate(zip(outputs,targets)):
		#import pdb;pdb.set_trace()
		for classid in output.keys():
			det = [0] * len(target[classid])
			class_recs[classid][num] = {'bbox': output[classid],
										'gt': target[classid],
										'detected': det}
	
	rec = np.zeros((num_classes))
	prec = np.zeros((num_classes))
	ttp = np.zeros((num_classes))
	ffp = np.zeros((num_classes))
	npos_valid = np.zeros((num_classes))
	info_all = np.zeros((num_classes,3,len(outputs)))


	keep = []
	for classid in range(num_classes):
		rec[classid], prec[classid], ttp[classid], ffp[classid], npos_valid[classid], info_all[classid] = calculate_metrics_class(class_recs[classid], ovthresh)
		if not np.isnan(rec[classid]):
			keep += [classid]
	
	return ttp.sum(), ffp.sum(), npos_valid.sum()

def get_accuracy(pred_boxes, target_boxes, pred_labels=None, target_labels=None, all_labels=None, do_classwise=False, ovthresh=0.5, template_2_class=None):

	'''
	This accuracy is calculated on single image
	'''

	if do_classwise:
		output_dict = dict()
		target_dict = dict()

		pred_labels_new = []
		pred_boxes_new = []

		
		for i,label in enumerate(pred_labels):
			#use it when doing calculation for pop clf
			
			if template_2_class is not None:
				pred_labels_new.append(template_2_class[label])

			else:
				pred_labels_new.append(label)
			
			pred_boxes_new.append(pred_boxes[i])

		pred_labels_new = np.array(pred_labels_new, dtype='str')
		pred_boxes_new = np.array(pred_boxes_new)
		target_labels = np.array(target_labels,dtype='str')
		target_boxes = np.array(target_boxes)
		 
		# import pdb;pdb.set_trace()
		for classid, label in enumerate(all_labels):
			#use for pop classifier
			#print ('PRED LABELS ',pred_labels_new,' LABEL ',label,' TARGET LABELS NEW ',target_labels)
			# import pdb;pdb.set_trace()
			output_dict[classid] = pred_boxes_new[pred_labels_new == label]
			target_dict[classid] = target_boxes[target_labels == label]  ###change for corrected labels

		# import pdb;pdb.set_trace()
		tp,fp,npos = get_acc_obj([output_dict], [target_dict], len(all_labels), ovthresh)
		#print ('TARGET BOX ',target_boxes.shape)
	else:
		tp,fp,npos = calculate_metrics_detection(target_boxes, pred_boxes, ovthresh)
		#print ('TARGET BOX FG BG ',target_boxes.shape)


	return tp,fp,npos
