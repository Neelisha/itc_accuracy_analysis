import os
import sys
import cv2
import pickle
import numpy as np
from PIL import ImageFile
from skimage.io import imread, imsave

from .accuracy_utils import *
from .utils import *

ImageFile.LOAD_TRUNCATED_IMAGES = True
sys.setrecursionlimit(100000)

'''
This logger will update number of boxes detected by each module
'''
class Basic_logger:
	def __init__(self, visualize=None):
		self.total = 0.
		self.info = dict()
		self.visualize = visualize

	def update(self, imname, boxes, labels, scores):
		self.total += len(boxes)
		self.info[imname] = {}
		self.info[imname]['boxes'] = boxes
		self.info[imname]['classes'] = labels
		self.info[imname]['scores'] = scores


	def show(self):
		status = "Total Predicted Boxes : {}".format(self.total)
		return status
	
	def save(self, name):
		if not os.path.isdir(os.path.dirname(name)):
			os.makedirs(os.path.dirname(name))
		pickle.dump(self.info, open(name, "wb"))

	def save_visualization(self, imname):
		pred_boxes, pred_labels, pred_scores = self.info[imname]
		try:
			ori_im = imread(imname, plugin='imageio')
			ori_im = np.ascontiguousarray(ori_im)
			for gbox,fgc_cname,score in zip(pred_boxes, pred_labels, pred_scores):
				ori_im = cv2.rectangle(ori_im, (gbox[0],gbox[1]), (gbox[2],gbox[3]), (255,0,0), 2)
				ori_im = cv2.putText(ori_im, '%s:%.2f'%(fgc_cname,score), (gbox[0],gbox[1]), 
									cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), thickness=1)
			imsave(os.path.join(self.visualize, imname.split("/")[-1]), ori_im)
		except:
			pass
	def save_text_extracted(self, imname, extracted_text):
		with open(os.path.join(self.visualize, 'text', imname.split("/")[-1].split('.')[0]+'.txt')) as f:
			for line in extracted_text:
				f.write('%s\n'%line)

'''
This logger will keep track of TP,FP,TN,FN on every image by each module
'''
class AccuracyLogger:
	def __init__(self, visualize=None, ovthresh=0.5, do_classwise=False, all_labels=None, template_2_class=None, fp_thresh=2,group_level=False):
		self.TP_all = 0.
		self.FP_all = 0.
		self.NPOS_all = 0.
		self.visualize = visualize
		self.ovthresh = ovthresh
		self.do_classwise = do_classwise
		self.all_labels = all_labels
		self.template_2_class = template_2_class
		self.num_imgs = 0
		self.nimgs_wo_fps = 0
		self.fp_thresh = fp_thresh
		self.group_level = group_level
		self.info = dict() # imname : [pred_boxes, gt_boxes, pred_labels, gt_labels, pred_scores]

	def update(self, imname, boxes, labels, scores, csv_path='random_csv.csv', extracted_text=None):
		#img, angle = read_image(imname)
		 
		try:
			# gt_boxes, gt_labels = get_gtboxes(imname, 0)
			gt_boxes, gt_labels = get_gtboxes_csv(imname, csv_path,self.group_level)

		except FileNotFoundError:
			gt_boxes = []
			gt_labels = []
		
		labels = [label.lower() for label in labels]
		gt_labels = [label.lower() for label in gt_labels]
		self.info[imname] = [boxes, gt_boxes, labels, gt_labels, scores]
		if self.visualize is not None:
			makedir(self.visualize)
			self.save_visualization(imname,self.template_2_class)
			if extracted_text is not None:
				makedir(os.path.join(self.visualize,'text'))
				self.save_text_extracted(imname, extracted_text)

		boxes, gt_boxes, labels, gt_labels = np.array(boxes), np.array(gt_boxes), np.array(labels), np.array(gt_labels)
		tp,fp,npos = get_accuracy(pred_boxes=boxes, target_boxes=gt_boxes, pred_labels=labels, target_labels=gt_labels, \
													all_labels=self.all_labels, do_classwise=self.do_classwise, ovthresh=self.ovthresh, template_2_class = self.template_2_class)
		if fp < self.fp_thresh:
			self.nimgs_wo_fps += 1
		self.TP_all += tp
		self.FP_all += fp
		self.NPOS_all += npos
		self.num_imgs += 1

	def show(self):
		prec = self.TP_all / (self.TP_all + self.FP_all)
		reca = self.TP_all / self.NPOS_all
		f1 = 2*prec*reca / (prec + reca) 
		status = "Precision : {}, Recall : {}, F1 : {}\n TP : {}, FP : {}, GT :{}, Img_count : {}, Imgs_wo_FP({}) : {}".format(
			prec, reca, f1, self.TP_all, self.FP_all, self.NPOS_all, self.num_imgs, self.fp_thresh, self.nimgs_wo_fps)
		return status

	def save(self, name):
		pickle.dump(self.info, open(name, "wb"))


	def save_visualization(self, imname,template_2_class=None):
		pred_boxes, gt_boxes, pred_labels, gt_labels, pred_scores = self.info[imname]
		try:
			ori_im = imread(imname, plugin='imageio')
			if ori_im.shape[-1] == 4:
				ori_im = ori_im[:,:,0:3]
			ori_im = np.ascontiguousarray(ori_im)
		
			if template_2_class!=None:
				# pred_labels_new = []
				# for label in pred_labels:
				# 	if 'icbu' in label.lower():
				# 		continue
				# 	pred_labels_new.append(self.template_2_class[label])
				#for gbox,fgc_cname,score in zip(pred_boxes, pred_labels_new, pred_scores):
				for gbox,fgc_cname,score in zip(pred_boxes, pred_labels, pred_scores):
					ori_im = cv2.rectangle(ori_im, (gbox[0],gbox[1]), (gbox[2],gbox[3]), (255,0,0), 2)
					ori_im = cv2.putText(ori_im, '%.2f:%s'%(score, template_2_class[fgc_cname]), (gbox[0],gbox[1]), 
											cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), thickness=1)
			else:
				#FOR TESTING SSD FROM POSITIVE SHEET
				if pred_scores is not None:
					for gbox,fgc_cname,score in zip(pred_boxes, pred_labels, pred_scores):
						ori_im = cv2.rectangle(ori_im, (gbox[0],gbox[1]), (gbox[2],gbox[3]), (255,0,0), 4)
						ori_im = cv2.putText(ori_im, '%.2f:%s'%(score, fgc_cname), (gbox[0],gbox[1]), 
												cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), thickness=4)
				else:
					for gbox,fgc_cname in zip(pred_boxes, pred_labels):
						ori_im = cv2.rectangle(ori_im, (gbox[0],gbox[1]), (gbox[2],gbox[3]), (255,0,0), 2)
						ori_im = cv2.putText(ori_im, '%s'%(fgc_cname), (gbox[0],gbox[1]), 
												cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), thickness=1)				

			for gbox,fgc_cname in zip(gt_boxes, gt_labels):
				ori_im = cv2.rectangle(ori_im, (gbox[0],gbox[1]), (gbox[2],gbox[3]), (0,0,255), 2)
				ori_im = cv2.putText(ori_im, '%s'%(fgc_cname), (gbox[0],gbox[1]), 
										cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255), thickness=1)

			imsave(os.path.join(self.visualize, imname.split("/")[-1]), ori_im)
		except:
			pass
	
	def save_text_extracted(self, imname, extracted_text):
		with open(os.path.join(self.visualize, 'text', imname.split("/")[-1].split('.')[0]+'.txt'), 'w') as f:
			for line in extracted_text:
				for l in line:
					f.write('{} '.format(l))
				f.write('\n')
