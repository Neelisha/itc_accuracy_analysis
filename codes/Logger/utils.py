import os,glob
import numpy as np

def makedir(foldername):
	if not os.path.isdir(foldername):
		os.makedirs(foldername)

def nms_inter(bboxes, thresh):
	if len(bboxes) == 0:
		return []

	areas = (bboxes[:,2]-bboxes[:,0])*(bboxes[:,3]-bboxes[:,1])
	keep = list(range(len(bboxes)))
	for i,bbox1 in enumerate(bboxes):
		for j,bbox2 in enumerate(bboxes[i+1:]):
			xmin = max(bbox1[0],bbox2[0])
			ymin = max(bbox1[1],bbox2[1])
			xmax = min(bbox1[2],bbox2[2])
			ymax = min(bbox1[3],bbox2[3])

			if xmax-xmin <0 or ymax-ymin <0:
				continue
			interA = (xmax-xmin)*(ymax-ymin)	#intersection area
			ov = float(interA) / (min(areas[i],areas[j+i+1]))
			if ov >= thresh:	
				to_remove = i if areas[i] < areas[j+i+1] else j+i+1		#remove the smallest box
				if to_remove in keep:
					keep.remove(to_remove)
	
	keep = np.asarray(keep)

	return keep

def get_image_pyramids(img, pyramid_scales):

	return [img.resize([int(i * s) for s in img.size]) for i in pyramid_scales]

def filter_boxes(boxes, imwidth, imheight):
	new_boxes = [ [max(0,box[0]), max(0,box[1]), min(box[2], imwidth), min(box[3], imheight)] for box in boxes]
	new_boxes = np.array(new_boxes)
	new_boxes = new_boxes[(new_boxes[:,2] - new_boxes[:,0])>0]
	new_boxes = new_boxes[(new_boxes[:,3] - new_boxes[:,1])>0] 
	return new_boxes

def box_nms(dets, scores, thresh):
    """Pure Python NMS baseline."""

    if len(dets) == 0:
    	return []

    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])
        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]

    return keep
	
def nms_inter_score(dets, scores, thresh=0.7):

    if len(dets) == 0:
    	return []

    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        min_area = np.minimum(areas[i], areas[order[1:]])
        ovr = inter / (min_area)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]

    return keep
