### General Imports
import sys
import glob
import pickle
import demjson
from tqdm import tqdm
import json

from skimage.io import imread
from collections import OrderedDict

from progressbar import ProgressBar  
# Config Imports
# from master import master_mapping_dict
import glob
from Logger.logger import *

import pandas as pd

 
import os
import numpy as np


gpd='general_pop_detector_frcnn'
pop='pop_classifier'
dsgn='pop_design_level_classifier'
nms='nms'


def nms_inter(bboxes, _,thresh):
	# thresh = config['NMS']['nms_inter_thresh']
	if len(bboxes) == 0:
		return []

	areas = (bboxes[:,2]-bboxes[:,0])*(bboxes[:,3]-bboxes[:,1])
	keep = list(range(len(bboxes)))
	for i,bbox1 in enumerate(bboxes):
		for j,bbox2 in enumerate(bboxes[i+1:]):
			xmin = max(bbox1[0],bbox2[0])
			ymin = max(bbox1[1],bbox2[1])
			xmax = min(bbox1[2],bbox2[2])
			ymax = min(bbox1[3],bbox2[3])

			if xmax-xmin <0 or ymax-ymin <0:
				continue
			interA = (xmax-xmin)*(ymax-ymin)    #intersection area
			ov = float(interA) / (min(areas[i],areas[j+i+1]))
			if ov >= thresh:    
				to_remove = i if areas[i] < areas[j+i+1] else j+i+1     #remove the smallest box
				if to_remove in keep:
					keep.remove(to_remove)
	
	keep = np.asarray(keep)

	return keep

def box_nms(dets, scores,thresh):
	"""Pure Python NMS baseline."""
	# thresh = config['NMS']['box_nms_thresh']
	if len(dets) == 0:
		return []

	x1 = dets[:, 0]
	y1 = dets[:, 1]
	x2 = dets[:, 2]
	y2 = dets[:, 3]

	areas = (x2 - x1 + 1) * (y2 - y1 + 1)
	order = scores.argsort()[::-1]

	keep = []
	while order.size > 0:
		i = order[0]
		keep.append(i)
		xx1 = np.maximum(x1[i], x1[order[1:]])
		yy1 = np.maximum(y1[i], y1[order[1:]])
		xx2 = np.minimum(x2[i], x2[order[1:]])
		yy2 = np.minimum(y2[i], y2[order[1:]])
		w = np.maximum(0.0, xx2 - xx1 + 1)
		h = np.maximum(0.0, yy2 - yy1 + 1)
		inter = w * h
		ovr = inter / (areas[i] + areas[order[1:]] - inter)
		inds = np.where(ovr <= thresh)[0]
		order = order[inds + 1]

	return keep


def process(boxes,classes,scores,thresh):
	 
	nms_modules_map = {
	'nms_inter' : nms_inter,
	'box_nms' : box_nms,
	}
	nms_modules = list()
	for k in ['nms_inter','box_nms' ]:
		nms_modules.append(k)

	for nms_module in nms_modules:
		try:
			thresh_ = thresh[nms_module]
			idx = nms_modules_map[nms_module](boxes, scores,thresh_)
			idx = np.array(idx)
			boxes, classes, scores = boxes[idx], classes[idx], scores[idx]
		except:
			pass

	return  boxes,   classes, scores



def correcting_pkls(pkl_path):
	'''
	function to correct the pkl as dictionary {'boxes':[],'classes':{},'scores':[]}
	'''
	paths = os.listdir(pkl_path)

	for vrsnid in paths: 
		pkls_lst = glob.glob(pkl_path+vrsnid+"/pkl/*")
		for pname in pkls_lst:	 
			df = pickle.load(open(pname,'rb'))
			imnames = list(df.keys())
			df2 ={}
			try:
				for imname in imnames:
					df2[imname]={}
					 
					df2[imname]['boxes'] = np.array(df[imname][0])
					df2[imname]['classes'] = np.array(df[imname][2])
					df2[imname]['scores'] = np.array(df[imname][4])

				with open( pname, 'wb') as handle:
					pickle.dump(df2, handle, protocol=pickle.HIGHEST_PROTOCOL)
			except Exception as e:
				pass
			 

def accurcay_thresh(projct_version_id,  tagged_sheets_path, unzip_path, Class_info_pkl, pkls, POP_THRESH,THRESH_NMS={'nms_inter':0.,'box_nms':0} ,fp_thresh=2,pnames=['general_pop_detector_frcnn','pop_classifier','nms']):
	''' Use MIN_MATCH id=f using key point classifier at design level'''
	
	#variables
	tagged_report_path = tagged_sheets_path +projct_version_id+'.csv'
	CLASS_CSV_PATH = unzip_path+'{}/inc_pipeline/class.csv'.format(projct_version_id)
	
	##loading pickle file
	all_info = dict()
	for pname in pnames: 
		all_info[pname] = pickle.load(open( pkls+'{}/pkl/{}.pkl'.format( projct_version_id, pname), 'rb'), encoding='latin1')


	##intialising all_label for accuracy logger
	template_2_class = dict()
	df = pd.read_csv(CLASS_CSV_PATH)
	df = df[pd.notnull(df['file_name'])]
	all_labels2 = []
	for index, row in df.iterrows():
		template_2_class[row['file_name'].lower()] = row['group_name'].lower() + '_' + row['class_name'].lower()
		all_labels2.append(row['group_name'].lower() + '_' + row['class_name'].lower())

	all_labels2 = list(set(all_labels2))
	pop_all_labels2 = pickle.load(open(Class_info_pkl, 'rb'))
	all_labels3 = list(pop_all_labels2.keys())

	all_labels =  all_labels2 + all_labels3

	all_labels_use = list(set(all_labels))
	#----------------------------------initialising accuracy_logger------------------------------------------------------------
	accuracy_logger = OrderedDict()
	 
	for i  in  pnames:	
		if i=='general_pop_detector_frcnn':
			accuracy_logger[i] = AccuracyLogger(visualize= None,do_classwise= False, all_labels= None, template_2_class= None,group_level= False,fp_thresh=fp_thresh)
			continue
		else:
			temp2cls = None#template_2_class
			group_level = False
			all_labels=  all_labels_use
		
		accuracy_logger[i] = AccuracyLogger(visualize= None,
								do_classwise= True,
								all_labels= all_labels,
								template_2_class= temp2cls,
								group_level=group_level,
								fp_thresh= fp_thresh)
	 
	## file names or images to process
	imnames=sorted(list(all_info[pnames[0]].keys()))
	#----------------------------accuracy update-----------------------------------------------
	pbar =ProgressBar()	
	for imname in  pbar(imnames):
		#GPD update
		if 'general_pop_detector_frcnn' in pnames:
			bgpd, cgpd, sgpd = all_info[gpd][imname]["boxes"], all_info[gpd][imname]["classes"], all_info[gpd][imname]["scores"]
			accuracy_logger[gpd].update(imname , bgpd ,  cgpd , sgpd ,tagged_report_path)
		 
		#thresholding on pop
		if 'pop_classifier' in pnames:
			bpop, cpop, spop = all_info[pop][imname]["boxes"], all_info[pop][imname]["classes"], all_info[pop][imname]["scores"]
 
		# box_pop,class_pop,score_pop=[],[],[]
			if len(spop)> 0:
				idx = np.where(spop>  POP_THRESH)
				accuracy_logger[pop].update(imname ,  bpop[idx],  cpop[idx],  spop[idx],tagged_report_path)
				#thresholding on pop design
				# box_pop =bpop[idx] #selected box after thresholding on pop
				# bdsgn, cdsgn, sdsgn = all_info[dsgn][imname]["boxes"], all_info[dsgn][imname]["classes"], all_info[dsgn][imname]["scores"]
				# bdsgn, cdsgn, sdsgn = all_info[dsgn][imname][0], np.array( all_info[dsgn][imname][2]), all_info[dsgn][imname][4]
				
				# idx=[]
				# for i in range(len(box_pop)):
				# 	if box_pop[i] in bdsgn:
				# 		idx.append(int(np.where(bdsgn==box_pop[i])[0].mean()))
				if len(idx)!=0:
					
					# bdsgn, cdsgn, sdsgn = bdsgn[idx], cdsgn[idx], sdsgn[idx] 
					# idx2 = np.where(sdsgn>MIN_MATCH)
					# accuracy_logger[ dsgn].update(imname ,  bdsgn[idx2], cdsgn[idx2], sdsgn[idx2],tagged_report_path)
					# thresholding after nms
					# boxes_nms,classes_nms,score_nms = process(bdsgn[idx2], cdsgn[idx2], sdsgn[idx2],THRESH_NMS)
					if 'nms' in pnames:
						boxes_nms,classes_nms,score_nms = process(bpop[idx],  cpop[idx],  spop[idx],THRESH_NMS)
						accuracy_logger[ nms].update(imname ,   boxes_nms,classes_nms,score_nms,tagged_report_path)
				else:
					if 'nms' in pnames:
						# accuracy_logger[ dsgn].update(imname ,  np.zeros(shape=(0,4)), [], [],tagged_report_path)
						accuracy_logger[nms].update(imname ,  np.zeros(shape=(0,4)), [], [],tagged_report_path)
					
			else:
				#no boxe is detected by pop  classifier for given image
				accuracy_logger[pop].update(imname ,  bpop, cpop, spop,tagged_report_path)
				# accuracy_logger[ dsgn].update(imname ,  np.zeros(shape=(0,4)), [], [],tagged_report_path)
				accuracy_logger[nms].update(imname ,  np.zeros(shape=(0,4)), [], [],tagged_report_path)


 
	df = pd.DataFrame()
	#nms

	for p in pnames:
		prec = accuracy_logger[p].TP_all / (accuracy_logger[p].TP_all + accuracy_logger[p].FP_all)
		reca = accuracy_logger[p].TP_all / accuracy_logger[p].NPOS_all
		f1 = 2*prec*reca / (prec + reca)
		df[p+'_f1'] =  [f1]
		df[p+'_prec'] = [ prec]
		df[p+'_recall'] = [ reca]
 
		 
		df[p+'_status'] =[accuracy_logger[p].show()]
	
	df['pop_thresh'] = [POP_THRESH]
	# df['min_mat_ch'] = [MIN_MATCH]
	df['nms_inter'] =[THRESH_NMS['nms_inter']]
	df['box_nms'] =[THRESH_NMS['box_nms']] 
		 
	return  df 
		

def save_acc_csv(tagged_sheets_path, unzip_path, Class_info_pkl, pkls, accurcaies_path, pop_thresh_ll=2, pop_thresh_ul=9,nmsinter_boxnms=[(0.7,0.3)],fp_thresh=2,pnames=['general_pop_detector_frcnn','pop_classifier','nms']):
	correcting_pkls(pkls)
	files = os.listdir(unzip_path)
	if not os.path.exists(accurcaies_path):
		os.makedirs(accurcaies_path)
	 
	best_df = pd.DataFrame()
	for projct_version_id in files:
		print(projct_version_id)
		final_df = pd.DataFrame()
		for z in (range(pop_thresh_ll,pop_thresh_ul)): 
			POP_THRESH = z*0.1
			for nms_inter_th,box_nms_th in nmsinter_boxnms:
				THRESH_NMS = {'nms_inter' :  nms_inter_th,
					'box_nms' :  box_nms_th,
					}
				df = accurcay_thresh(projct_version_id, tagged_sheets_path, unzip_path, Class_info_pkl, pkls, POP_THRESH,THRESH_NMS,fp_thresh,pnames)
				final_df = pd.concat([final_df,df]).reset_index(drop=True)
		final_df.to_csv(accurcaies_path +projct_version_id +"_accu.csv",index=False)


def total_accuracy_itc(df,pname,fp_thresh=2):

	nlots = df.shape[0]
	TP = 0.
	FP = 0.
	GT = 0.
	Image_Count = 0
	Image_Count_FP = 0

	for i in range(nlots):
		l = df[pname+"_status"].iloc[i].split(",")
		tp = float(l[2].split(":")[-1])
		fp = float(l[3].split(":")[-1])
		gt= float(l[4].split(":")[-1])
		img_count= float(l[5].split(":")[-1])
		image_count_fp= float(l[6].split(":")[-1])
		if tp == np.NaN:
			tp=0
		if fp ==np.NaN:
			fp =0
		if gt ==np.NaN:
			gt = 0
		if img_count ==np.NaN:
			img_count = 0
		if image_count_fp ==np.NaN:
			image_count_fp = 0
		TP=TP+tp
		FP = FP+fp
		GT= GT+gt
		Image_Count = Image_Count+img_count
		Image_Count_FP = Image_Count_FP+image_count_fp
	print(nlots,TP,FP,GT)
	prec =  TP /(TP+FP)
	reca =  TP / GT
	f1 = 2*prec*reca / (prec + reca)

	status = "Precision : {}, Recall : {}, F1 : {}\n TP : {}, FP : {}, GT :{}, Image_Count :{},  Imgs_wo_FP({}) :{}".format(prec, reca, f1,  TP , FP,  GT, Image_Count,fp_thresh, Image_Count_FP)

	return f1, prec, reca,status


def combine_results(Pathlotwise_acc_csv):
	project_version_id = os.listdir(Pathlotwise_acc_csv)
	combine_df = pd.DataFrame()
	for f in project_version_id:
		try: 
			if f!='combine_accu.csv':
				df = pd.read_csv(Pathlotwise_acc_csv+f)
				df['lot_name'] = [ f]*len(df)
				combine_df = pd.concat([combine_df,df]).reset_index(drop=True)
		except:
			pass
	combine_df['pop_thresh']=np.round(combine_df['pop_thresh'],2)
	combine_df.to_csv(Pathlotwise_acc_csv+"combine_accu"+ ".csv",index=False)
	return combine_df



if __name__ == '__main__':

	tagged_sheets_path='/home/pdguest/Neelisha/CV/ITC/dec_tagged_report/'
	unzip_path ='/home/pdguest/Neelisha/CV/ITC/dec_unzip/'
	Class_info_pkl='/home/pdguest/Neelisha/CV/ITC/all-in-1-combined/Weights/pop_classifier/designlevel/class_info.pkl'
	pkls ="/home/pdguest/Neelisha/CV/ITC/all-in-1-combined/runs/"
	accurcaies_path = 'accuracies/'
	
	pop_thresh_ll = 2
	pop_thresh_ul = 9
	nmsinter_boxnms = [(0.7,0.3)]
	fp_thresh= 2

	# to save accuracies on different threshold for every lot
	save_acc_csv(tagged_sheets_path,unzip_path,Class_info_pkl,Class_info_pkl,pkls,accurcaies_path,pop_thresh_ll,pop_thresh_ul,nmsinter_boxnms,fp_thresh)
	
	#combining accuracies for all lots on every thresholds 
	combine_df = combine_results(accurcaies_path)
	 
	pop_thresh =  [i for i in range(pop_thresh_ll,pop_thresh_ul)]
	 
	total = pd.DataFrame()
	for p in  pop_thresh:
		for nms_interth,bnms  in nmsinter_boxnms: 
			df2 = pd.DataFrame()
			df2['lot_name'] = ["Total"]
			df2['pop_thresh']= [p*0.1]
			df2['nms_inter'] =  [nms_interth]
			df2['box_nms'] =  [bnms]
			for pname in ['general_pop_detector_frcnn','pop_classifier','nms']:
				df_=combine_df[(combine_df['pop_thresh']== round(p*0.1,2)) & (combine_df['nms_inter']== nms_interth) & (combine_df['box_nms']== bnms)].reset_index(drop=True)  
				 
				f1,prec,reca,status = total_accuracy_itc(df_.copy(),pname)
				# df2['min_match']=[m]
				df2[pname+"_f1"] = [f1]
				df2[pname+"_prec"] = [prec]
				df2[pname+"_recall"] = [reca]
				df2[pname+"_status"] = [status]
			total = pd.concat([df2,total]).reset_index(drop=True)	 
	
	# merging total accuracies on combined_df
	combine_df =  pd.concat([combine_df,total]).reset_index(drop=True)
	combine_df.to_csv(accurcaies_path+"combine_accu.csv",index=False)
 
		 
		 
	
